#include <16f877.h>
#device ADC=8, //Especificamos el convertidor analogico digital con 8 bits
#fuses HS,NOPROTECT, //habilitamos la comunicacion serie
//en modo asincrono en una velocida alta
#use delay(clock=20000000) //Valor que va a tomar el oscilador
#use rs232(baud=38400, xmit=PIN_C6, rcv=PIN_C7) //Transmision sr232 PIN transmision y recepcion
#org 0X1F00,0x1FFF void loader16F877(void){} //for
int var1;
#int_rb //Funcion de interrupcion
//Cambios de niveles d elos bits m�s significativos
int_p(){
  //Detecci�n del flaco de subida para B4 de PORTB
  if(input(pin_b4))
    printf("\nPB4 activado");
  //Detecci�n del flaco de bajada para B4 de PORTB
  else
    printf("\nPB4 desactivado");
    
  //Detecci�n del flaco de subida para B5 de PORTB
  if(input(pin_b5))
    printf("\nPB5 activado");
  //Detecci�n del flaco de bajada para B5 de PORTB
  else
    printf("\nPB5 desactivado");
    
  //Detecci�n del flaco de subida para B6 de PORTB
  if(input(pin_b6))
    printf("\nPB6 activado");
  //Detecci�n del flaco de bajada para B6 de PORTB
  else
    printf("\nPB6 desactivado");
    
  //Detecci�n del flaco de subida para B7 de PORTB
  if(input(pin_b7))
    printf("\nPB7 activado");
  //Detecci�n del flaco de bajada para B7 de PORTB
  else
    printf("\nPB7 desactivado");

  printf("\n\n-----------------------------\n\n");
  
}

void main(){
  //Conguraci�n del pre escalador=256 y las interrupciones 
  setup_counters(RTCC_INTERNAL,RTCC_DIV_256); 
  //Habilita interrupcion por timer 0
  enable_interrupts(INT_RTCC); 
  //Habilita todas las interrupciones que existen
  enable_interrupts(GLOBAL);
  //Lectura continua
   while(1){
     //Se guarda en la variable el valor de entrada del portb
     var1=input_b();
     //Se manda como salida a porta el valor de la variable
     output_a(var1);
   }
}
