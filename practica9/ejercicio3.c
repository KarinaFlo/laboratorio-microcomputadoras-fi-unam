#include <16f877.h> 
#device ADC=8, //Especificamos el convertidor analogico digital con 8 bits
#fuses HS,NOPROTECT, //habilitamos la comunicacion serie
//en modo asincrono en una velocida alta
#use delay(clock=20000000) //Valor que va a tomar el oscilador 
#use rs232(baud=38400, xmit=PIN_C6, rcv=PIN_C7) //Transmision sr232 PIN transmision y recepcion
#org 0X1F00,0x1FFF void loader16F877(void){} //for
int conv; //declaraci�n de variable 
long cont=0; //contador para activar la interrupcion

//funcion de interrupcion con todas las funciones habilitadas
#int_RTCC
//activamos la funci�n que va a servir como cronometros
clock_isr(){ //para usar el timer0
   cont++; //incrementamos contador
   /*Determinamos 10 segundos a trav�s del calculo del numero de interrupciones necesarias
   Preescalador*TCI=256*0.2=51.2ms
   51.2*256=13.1msg es lo que tarda cada interrupci�n 
   Para conseguir las 10 interrupciones --> 769 interrupciones * 13.1 ms = 10 segundos
   */
   if(cont==769*3){ //se multiplica por tres para obtener 30 segundos
     //Se imprime en pantalla cada 30 segundos de interrupcion
     printf("Laboratorio de microcomputadoras\n");
     cont=0;
   }
}

void main(){
  //Inicializamos el cronometro en cero
  set_timer0(0); 
  //Conguraci�n del pre escalador=256 y las interrupciones 
  setup_counters(RTCC_INTERNAL,RTCC_DIV_256); 
  //Habilita interrupcion por timer 0
  enable_interrupts(INT_RTCC); 
  //Habilita todas las interrupciones que existen
  enable_interrupts(GLOBAL);
  //Equivalente a colocar OOH en ADCON1 para determinar que sera analogico
  setup_port_a(ALL_ANALOG);
   /*A partir de aqu� es quivalente a realizar la configuraci�n del ADCON en ensamblador */
   //Seleccionar el oscilador interno para que trabaje con la frecuencia del reloj
  setup_adc(ADC_CLOCK_INTERNAL);
  //Especifica el canal con el que se va a trabajar
  set_adc_channel(2);

  while(1){
    //Retardo en ensamblador para que se pueda realizar la conversion
    //2ms de retardo
    delay_us(20);
    /*Equivalente a leer el registro ADDRESS en ensamblador, el cual almacena
    el resultado de la conversi�n. En este caso se lee la conversi�n y se almacena
    en la variable converse*/
    conv=read_adc();
    //Se imprime en pantalla
    printf("el resultado es: =%x\n",conv); 
   /*Mandar al puerto B la conversion
   Equivalente a pasar el resultado al registro w y luego a portb en ensamblador*/
    output_b(conv); //Salida en puerto B 
  }
}

