#include <16f877.h> 
#device ADC=8, //Especificamos el convertidor analogico digital con 8 bits
#fuses HS,NOPROTECT, //habilitamos la comunicacion serie
//en modo asincrono en una velocida alta
#use delay(clock=20000000) //Valor que va a tomar el oscilador 
#use rs232(baud=38400, xmit=PIN_C6, rcv=PIN_C7) //Transmision sr232 PIN transmision y recepcion
#org 0X1F00,0x1FFF void loader16F877(void){} //for
int converse;
  void main(){
    /*Equivalente a realizar la configuración del ADCON0 en ensamblador
    */
    //Equivalente a colocar ceros en ADCON0 para determinar que sera analogico
    setup_port_a(ALL_ANALOG); 
    //Equivalente a seleccionar el oscilador interno para que trabaje con la
    //frecuencia del reloj
    setup_adc(ADC_CLOCK_INTERNAL); 
    //Especifica el canal con el que se va a trabajar
    set_adc_channel(0); //Habilitar canal 0

    while(1){
      //Retardo en ensamblador para que se pueda realizar la conversion
      //2ms de retardo
      delay_ms(20); // retardo de 20ms
      /*Equivalente a leer el registro ADDRESS en ensamblador, el cual almacena
      el resultado de la conversión. En este caso se lee la conversión y se almacena
      en la variable converse*/
      converse=read_adc(); //guardamos conversion en converse
      printf("El resultado es:=%x\n", converse);//impresión en pantalla
      /*Mandar al puerto B la conversion
      Equivalente a pasar el resultado al registro w y luego a portb en ensamblador.
      */
      output_b(converse); s
    }

  }
