 processor 16f877 ;Se define el PIC a trabajar
 include<p16f877.inc> ;Se carga la biblioteca para el PIC
;Se reserva espacio en para contador en la direcci�n de memoria 20
contador equ h'20' 
;Se reserva espacio para valor1 en la direcci�n 21
valor1 equ h'21'
;Se reserva espacio para valor2 en la direcci�n 22
valor2 equ h'22'
;Se reserva espacio para valor3 en la direcci�n 23
valor3 equ h'23'
;ct1 toma el valor de 40h
cte1 equ 40h
;ct2 toma el valor de 100h
cte2 equ 100h
;ct3 toma el valor de 120h
cte3 equ 120h
 ;Carga vector de reset
 org 0
 ;Llamada a la rutina de inicio
 goto inicio
 ;Direcci�n de inicio del programa
 org 5
 
;Definici�n de la rutina de inicio
inicio:
;Nos movemos al banco1 para usar TRIS
;El cambio de banco se hace con RP0 Y RP1 de STATUS
;(RP1,RP0) = (0,1) = Banco1
 ;Se coloca un 1 al bit 5 del registro STATUS
 ;bit5 = RP0
 bsf STATUS,5
 ;Se coloca un 0 al bit 6 del registro STATUS
 ;bit6 = RP1
 BCF STATUS,6
 ;Se configura registro w
 MOVLW H'0'
 ;Asignamos al puerto B con 0 en todos su registro TRIS
 ;Con esto hacemos que el registro completo sea de salida
 MOVWF TRISB
 ;Nos cambiamos al banco 0 para visualizar la salida del PORTB
 ;RP0 se pone en 0
 ;(RP1,RP0) = (0,0) = Banco0
 BCF STATUS,5
 ;Limpia el puerto B, listo para usarse
 clrf PORTB
 
;Simula en encendido y apagado de un led
;El bit0 representa el led
loop2:
 ;Se colocan los 8 bits encendidos en W para PORTB
 movlw b'11111111'
 ;Se encienden todos los bits del PORTB
 movwf PORTB
 ;Llamada a la rutina de retardo para poder visualizar el encendido
 call retardo
 ;Se apagan todos los bits del PORTB
 clrf PORTB
 ;Llamada a la rutina de retardo
 call retardo
 ;Llamada a la rutina de loop2 para que este prendiendo y apagando continuamente
 goto loop2
 
;Permite un retardo para visualizar el prendido
;y apagado del led
;El retardo es de 30 segundos
retardo:
 ;El valor de la cte1 se pasa al registro w
 movlw cte1
 ;valor1 toma el valor de cte1 con ayuda de w
 movwf valor1
;Definicion de la rutina tres
tres:
 ;El valor de la cte2 se pasa al registro w 
 movlw cte2
 ;valor2 toma el valor de cte2 con ayuda de w
 movwf valor2
;Definicion de la rutina dos
dos
 ;El valor de la ct3 se pasa al registro w
 movlw cte3
 ;valor3 toma el valor de ct3 con ayuda de w
 movwf valor3
;Definicion de la rutina uno
uno 
 ;Realiza el decremento del valor3
 decfsz valor3
 ;Llamada a la rutina uno
 goto uno
 ;Realiza el decremento del valor2
 decfsz valor2
 ;Llamada a la rutina dos
 goto dos
 ;Realiza un decremento del valor1
 decfsz valor1
 ;Llamada a la rutina tres
 goto tres
 return
 END ;Directiva de fin del programa
