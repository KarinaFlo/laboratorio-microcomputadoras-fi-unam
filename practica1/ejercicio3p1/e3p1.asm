;Laboratorio de microcomputadoras
;Practica 1 ejercicio 3
include <p16f877.inc>;Biblioteca para el PIC16F877
    A equ H'20' ;Se asigna el espacio de memoria 20 a la variable A
    N equ H'21' ;Se asigna el espacio de memoria 21 a la variable N
;Para resetear el microprocesador
org 0
goto inicio ;Llama a la funcion inicio
org 5
inicio: ;Declaracion de la funci�n inicio
	movlw h'07' ;Se asigna el valor de 7 a W porque es la cantidad de saltos que se piden
	movwf N;Se asigna el 7 a N
	movlw h'01' ;Se asigna el valor de 1 a W para inciar la secuencia que ser� un corrimiento de bits izquierda
	movwf A ;Se asigna el valor de 1 a A
ciclo: ;Declaracion de la  funcion ciclo
	rlf A,1 ;Se hace el corrimiento de un bit a la izquierda
	decf N ;Se decrementa el valor de N en 1
	btfss STATUS,2;Permite dar un salto si la bandera Z=1
goto ciclo ;Regresa a la funcion ciclo
goto inicio
end
