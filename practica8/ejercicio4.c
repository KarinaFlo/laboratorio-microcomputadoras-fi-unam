#include <16f877.h> 
#fuses HS,NOPROTECT, //habilitamos la comunicacion serie
#use delay(clock=20000000) //Valor que va a tomar el oscilador 
/*el baud=38400
 pin_c6,pin_c7 = habilitan la comunicacion de transminision,recepcion
*/
#use rs232(baud=38400, xmit=PIN_C6, rcv=PIN_C7)  
#org 0x1F00, 0x1FFF void loader16F877(void) {} //for the 8k 16F876/7 
void main(){ 
while(1){ 
/*
Nos cambiamos de banco a RP1 y configuramos el PORTB con
ADCON1 y TRISA como salida.
Cargamos el valor 0x01 al PORTB.
Cargamos el bit '11111111' en w y lo asignamos a PORTB 
para prender todos los leds.
*/
output_b(0xff);//           
printf(" Todos los bits encendidos \n\r");   
/*
Esta instrucci�n es equivalente a crear un delay en esamblador
tomando en cuenta el tiempo del oscilador.
As� obtenemos un retardo para la ejecuci�n de la siguiente
instrucci�n.
*/
delay_ms(1000); 
/*
Cargamos en w los bits en cero para pasarlos al PORTB
Lo anterior apaga los leds.
*/
output_b(0x00);  
printf(" Todos los leds apagados \n\r");  
/*
Se realiza un retardo con la misma l�gica que el anterior
*/
delay_ms(1000);            
}//while 
}//main


