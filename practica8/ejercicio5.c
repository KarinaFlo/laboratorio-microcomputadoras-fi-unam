#include <16f877.h> 
#include <stdio.h>
#include <stdlib.h>
#fuses HS,NOPROTECT, //habilitamos la comunicacion serie
#use delay(clock=20000000)  //Valor que va a tomar el oscilador 

#use rs232(baud=38400, xmit=PIN_C6, rcv=PIN_C7)  
#org 0x1F00, 0x1FFF void loader16F877(void) {} //for the 8k 16F876/7 
void main(){ 
//Declaracion de variables
char x;
int i, Dato;
while(1){ 
/*
Se lee la entrada del teclado y se almacena en 
la variable x con ayuda de la funci�n getch.
En ensamblador deb�amos cargarlo a W y posteriormente evaluar.
*/
x=getch();
putc(x);
switch(x){ 
      case 0:
      /*
      Nos cambiamos de banco a RP1 y configuramos el PORTB con
      ADCON1 y TRISA como salida.
      Cargamos en w los bits en cero para pasarlos al PORTB
      Lo anterior apaga los leds.
      */
      output_b(0x00);//
      printf("Todos apagados");
      break;
   case 1:
      /*
      Nos cambiamos de banco a RP1 y configuramos el PORTB con
      ADCON1 y TRISA como salida.
      Cargamos el valor 0x01 al PORTB.
      Cargamos el bit '11111111' en w y lo asignamos a PORTB 
      para prender todos los leds.
      */
      output_b(0xff);//
      printf("Todos encendidos");
      break;
   case 2:
      printf("Corrimiento a la izquierda");
      /*
      Asignamos 0x01 a Dato. Equivalente a almacenarlo en w.
      */
      Dato = 0x01;
      /*
      Pasamos el valor de dato al PORTB.
      Equivalente a pasar w al PORTB.
      */
      output_b(Dato);
      /*
      Esta instrucci�n es equivalente a crear un delay en esamblador
      tomando en cuenta el tiempo del oscilador.
      As� obtenemos un retardo para la ejecuci�n de la siguiente
      instrucci�n.
      */
      Delay_ms(500);
      for(i = 0;i<7;i++){
      //RLF = Corrimiento a la izquierda
         Dato=(Dato << 1);
         /*
         Pasamos el valor de dato al PORTB.
         Equivalente a pasar w al PORTB.
         */
         output_b(Dato);
         /*
         Se realiza un retardo con la misma l�gica que el anterior
         */
         Delay_ms(500);
      }
      break;
   case 3:
      printf("Corrimiento a la derecha");
      /*
      Asignamos 0x80 a Dato. Equivalente a almacenarlo en w.
      */
      Dato = 0x80;
      /*
      Pasamos el valor de dato al PORTB.
      Equivalente a pasar w al PORTB.
      */       
      output_b(Dato);
      /*
      Se realiza un retardo con la misma l�gica que el anterior
      */
      Delay_ms(500);
      for(i = 0;i<7;i++){
      //RRF = Corrimiento a la derecha
         Dato=(Dato >> 1);
         /*
         Pasamos el valor de dato al PORTB.
         Equivalente a pasar w al PORTB.
         */
         output_b(Dato);
         /*
         Se realiza un retardo con la misma l�gica que el anterior
         */
         Delay_ms(500);
      }
      break;
   case 4:
   printf("Doble corrimiento");
      /*
      Asignamos 0x01 a Dato. Equivalente a almacenarlo en w.
      */
      Dato = 0x01;
      output_b(Dato);
      /*
      Se realiza un retardo con la misma l�gica que el anterior
      */
      Delay_ms(500);
      for(i = 0;i<7;i++){
      //RRF = Corrimiento a la derecha
         Dato=(Dato << 1);
         /*Asignamos el dato en el registro W
         para despues pasarlo al puerto B
         */
         output_b(Dato);
         /*
         Se realiza un retardo con la misma l�gica que el anterior
         */
         Delay_ms(500);
      }
      /*
      Asignamos 0x80 a Dato. Equivalente a almacenarlo en w.
      */
      Dato = 0x80;
      /*
      Pasamos el valor de dato al PORTB.
      Equivalente a pasar w al PORTB.
      */    
      output_b(Dato);
      /*
      Se realiza un retardo con la misma l�gica que el anterior
      */
      Delay_ms(500);
      for(i = 0;i<7;i++){
         Dato=(Dato >> 1);
         /*Asignamos el dato en el registro W
         para despues pasarlo al puerto B
         */
         output_b(Dato);
         /*
         Se realiza un retardo con la misma l�gica que el anterior
         */
         Delay_ms(500);
      }
      break;
   case 5:
      /*
      Nos cambiamos de banco a RP1 y configuramos el PORTB con
      ADCON1 y TRISA como salida.
      Cargamos el valor 0x01 al PORTB.
      Cargamos el bit '11111111' en w y lo asignamos a PORTB 
      para prender todos los leds.
      */
      output_b(0xff);//           
      printf(" Todos los bits encendidos \n\r"); 
      /*
      Se realiza un retardo con la misma l�gica que el anterior
      */
      delay_ms(500);
      /*
      Nos cambiamos de banco a RP1 y configuramos el PORTB con
      ADCON1 y TRISA como salida.
      Cargamos en w los bits en cero para pasarlos al PORTB
      Lo anterior apaga los leds.
      */
      output_b(0x00);  
      printf(" Todos los leds apagados \n\r");  
      /*
      Se realiza un retardo con la misma l�gica que el anterior
      */
      delay_ms(500); 
      break;
   
}//switch            
}//while 
}//main0
