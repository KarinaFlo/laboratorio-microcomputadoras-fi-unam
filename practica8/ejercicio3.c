#include <16f877.h> 
#fuses HS,NOPROTECT, //habilitamos la comunicacion serie
//en modeo asincrono en una velocida alta
#use delay(clock=20000000) //Valor que va a tomar el oscilador 
#org 0x1F00, 0x1FFF void loader16F877(void) {} //for the 8k 16F876/7 
int var1;//Declaracion de variable
void main(){
while(1){ 
/*
PORTA se tiene que configurar el ADCON1
La configuracion tendra el valor 6 o 5
*/
var1=input_a();//Equivalente a AND
/*
Nos cambiamos de banco a RP1 y configuramos el PORTB con
ADCON1 y TRISA como salida.
Cargamos el valor 0x01 al PORTB.
Cargamos el bit '11111111' en w y lo asignamos a PORTB 
para prender todos los leds.
*/
output_b(var1); 
}//while 
}//main


