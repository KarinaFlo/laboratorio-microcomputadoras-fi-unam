processor 16f877 
include <p16f877.inc> 

Y equ h'20' 
X equ h'21' 
G equ h'22' 

org 0 
goto inicio 
org 5 

inicio: 
;Nos cambiamos de banco
BSF STATUS,RP0 
BCF STATUS,RP1  
CLRF TRISB; ;Limpiamos TRISB
CLRF ADCON1 ;Limpiamos el ADCON1 
BSF TXSTA,BRGH ;Bit para validar la tasa de transferencia
MOVLW D'32' ;El 32 indicar� la Baud rate
MOVWF SPBRG ;se asignan el valor 32 al bit SPBRG
BCF TXSTA,SYNC ;;Indicamos que ser� de modo as�crono 
BSF TXSTA,TXEN ;Habilitamos la transmision de datos 
;Aqui termina la configuracion
;Comienza configuracion del puerto serie
  ;Cambio de banco
BCF STATUS,RP0 ;Nos cambiamos de banco
;Configuracion del registro RCSTA
BSF RCSTA,SPEN ;Habilitamos el puerto serie
BSF RCSTA,CREN ;Habilitamos el receptor 
 
CICLO: ;configurar CAD 
MOVLW H'C1' ;agregamos a W los bits 11000001
MOVWF ADCON0 ;se configura los bits de ADCON1

LEER: 
BSF ADCON0,2 ;Se habilita el ADCON0
CALL RETARDO ;se hace un retardo

ESPERA: 
BTFSC ADCON0,2 
GOTO ESPERA 
MOVF ADRESH,W ;ADRESH guardara el resultado de la transmision
MOVWF X ;se guarda en X

CICLOY: 

MOVLW H'C9' ;agregamos a W los bits 11001001
MOVWF ADCON0;Se configura el ADCON0 con los bits anteriores  

LEERY: 
BSF ADCON0,2 ;Se lee el adcon0
CALL RETARDO ;se hace un retardo

ESPERAY: 
BTFSC ADCON0,2 
GOTO ESPERAY 
MOVF ADRESH,W ;ADRESH guardara el resultado de la transmision
MOVWF Y ;Se guarda en Y
;comparaciones x-y 
UNO: 
MOVF X,W ;Guardamos lo de X en W
SUBWF Y ;comparamos con Y
BTFSS STATUS,C  ;Si Y es m�s grande que X
GOTO DOS ;se va a DOS
MOVLW A'1' 
MOVWF TXREG ;Transmite que esta en el canal 1
BSF STATUS,RP0 ;Preparamos la recepcion
GOTO TRANSMITE ;Vamos a la funcion transmite
 
DOS: 
MOVLW A'2' ;Si el canal 2 fue mayor
MOVWF TXREG  ;Transmite que esta en el canal 1
BSF STATUS,RP0 ;preparamos la recepcion

TRANSMITE: 
BTFSS TXSTA,TRMT ;Verificamos si ya se transmitio un dato
GOTO TRANSMITE ;Falta transmitir
BCF STATUS,RP0 ;Prepara la recepcion  
GOTO CICLO ;vuelve al ciclo

RETARDO: 
MOVLW H'30' ;Hacemos un retardo 
MOVWF G 

LOOP: 
DECFSZ G 
GOTO LOOP ;Regresa a Loop
RETURN 

END
