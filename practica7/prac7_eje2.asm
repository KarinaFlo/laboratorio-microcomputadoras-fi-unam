processor 16f877
include<p16f877.inc>

Resp equ h'20'
org 0H
goto INICIO
org 05H

INICIO:
  ;Cambio al banco 1
  BSF STATUS,RP0 
  ;Cambio al banco 1
  BCF STATUS,RP1
  ;Configura una tranmision asincrona
  BSF TXSTA,BRGH 
  ;Limpia TRISB
  CLRF TRISB 
  
;Se guarda 32 en w
MOVLW D'32'
;Velocidad del transmisor 
MOVWF SPBRG 
BCF TXSTA,SYNC
;Se habilita el transmisor para que empiece a trabajar
BSF TXSTA,TXEN 
;Aqui termina la configuracion

;Comienza configuracion del puerto serie
  ;Cambio de banco
  BCF STATUS,RP0
  ;Configuracion del registro RCSTA
  BSF RCSTA,SPEN
  
RECIBE:
  ;Se verifica que llega un dato
  BTFSS PIR1,RCIF 
  GOTO RECIBE
  ;Se guarda el dato en el registro w 
  MOVF RCREG,W 
  ;Guarda el resultado
  MOVWF Resp 
  
CICLO:
  ;Se limpia PORTB
  CLRF PORTB 
  ;Limpia la bandera z
  BCF STATUS,Z
  ;Se recibe lo del teclado en w 
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'A' 
  BTFSC STATUS,Z
  ;Si el ASCCI es una A despliega A
  GOTO LETRAA 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'E'
  BTFSC STATUS,Z ;
  ;Si el ASCCI es una E despliega E
  GOTO LETRAE 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'I' 
  BTFSC STATUS,Z ;
  ;Si el ASCCI es una I despliega I
  GOTO LETRAI
  ;Se recibe lo del teclado en w 
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'O'  
  BTFSC STATUS,Z ;
  ;Si el ASCCI es una O despliega O
  GOTO LETRAO 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'U'  
  BTFSC STATUS,Z 
  ;Si el ASCCI es una U despliega U
  GOTO LETRAU 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0  
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'a'  
  BTFSC  STATUS,Z  
  ;Si el ASCCI es una A minuscula despliega a
  GOTO LETRAAm 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0  
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'e'  
  BTFSC STATUS,Z 
  ;Si el ASCCI es una E minuscula despliega e
  GOTO LETRAEm 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0  
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'i'   
  BTFSC STATUS,Z  
  ;Si el ASCCI es una I minuscula despliega i
  GOTO LETRAIm 
  ;Se recibe lo del teclado en w 
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'o' 
  BTFSC STATUS,Z 
  ;Si el ASCCI es una O minuscula despliega o 
  GOTO LETRAOm 
  ;Se recibe lo del teclado en w  
  MOVF Resp,0 
  ;Comparacion del dato recibido en ASCCI
  SUBLW A'u' 
  BTFSC STATUS,Z
  ;Si el ASCCI es una U minuscula despliega u   
  GOTO LETRAUm 

LETRAA  
  CLRF PORTB ;se limpia el puerto b  
  movlw h'f7';C�DIGO EN HEXADECIMAL QUE MARCA AL DISPLAY PARALA LETRAA  
  MOVWF PORTB ;se guarda en w 
  goto RECIBIR2 ;se va a recibir2

LETRAE  
  CLRF PORTB ;se limpia el puerto b
  movlw h'f9' ;C�DIGO EN HEXADECIMAL QUE MARCA AL DISPLAY PARALA LETRAE  
  MOVWF PORTB ;se guarda en w
  goto RECIBIR2 ; se va a recibir2

LETRAI  
  CLRF PORTB  ;se limpia el puerto b
  movlw h'86' ;C�DIGO EN HEXADECIMAL QUE MARCA AL DISPLAY PARALA LETRAI  
  MOVWF PORTB ;se guarda en w
  goto RECIBIR2 ;se va a recibir2

LETRAO  
  CLRF PORTB ;se limpia el puerto b
  movlw h'Bf';C�DIGO EN HEXADECIMAL QUE MARCA AL DISPLAY PARALA LETRAO
  MOVWF PORTB ;se guarda en w 
  goto RECIBIR2 ;se va recibir2

LETRAU  
  CLRF PORTB ;se limpia el puerto b
  movlw h'BE';C�DIGO EN HEXADECIMAL QUE MARCA AL DISPLAY PARALA LETRAU  
  MOVWF PORTB  ;se guarda en w
  goto RECIBIR2  ;se va a recibir2

;Configuracion para el display 7 segmentos.  
LETRAAm  
  CLRF PORTB ;se limpia el puerto b
  movlw b'11011111' ; c�digo en binario para mandar una a minuscula  
  MOVWF PORTB ;se guarda en w
  goto RECIBIR2 ;se va a recibir2

LETRAEm  
  CLRF PORTB ;se limpia el puerto b 
  movlw b'11111011' ; c�digo en binario para mandar una e minuscula  
  MOVWF PORTB ;se guarda en w 
  goto RECIBIR2  ;se va a recibir2

LETRAIm  
  CLRF PORTB ;se limpia el puerto b   
  movlw b'10000100' ; c�digo en binario para mandar una i minuscula  
  MOVWF PORTB ;se guarda en w 
  goto RECIBIR2 ;se va a recibir2 

LETRAOm  
  CLRF PORTB ;se limpia el puerto b  
  movlw b'11011100'; c�digo en binario para mandar una o minuscula  
  MOVWF PORTB ;se guarda en w   
  goto RECIBIR2 ;se va a recibir2  

LETRAUm  
  CLRF PORTB ;se limpia el puerto b    
  movlw b'10011100' ; c�digo en binario para mandar una u minuscula  
  MOVWF PORTB ;se guarda en w    
  goto RECIBIR2 ;se va a recibir2   

RECIBIR2  
  MOVF RCREG, W ;Se guarda el dato recibido en w
  MOVWF TXREG ; Carga W al registro de transmision  
  BSF STATUS,RP0;Cambio de banco para verificar transmision  

TRANSMITIR   
  BTFSS TXSTA, TRMT;Verificamos si ya se transmitio un dato   
  ;trmt=0 ->Falta transmitir  
  ;trmt=1 ->completo la transmision  
  GOTO TRANSMITIR  ;Falta transmitir
  BCF STATUS,RP0;Prepara la recepcion  
  GOTO RECIBE ;Transmisi�n completa
End  
  
