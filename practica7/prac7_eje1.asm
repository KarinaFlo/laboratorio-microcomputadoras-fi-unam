processor 16f877 
include<p16f877.inc> 
;Declaraci�n de espacios de memoria 
G equ 0x20 ;
Y equ 0x24 
X equ 0x25 
VALOR1 equ H'21' 
VALOR2 equ H'22' 
VALOR3 equ H'23' 
cte1 equ 20H 
cte2 equ 50H 
cte3 equ 60H 

org 0H 
goto inicio 
org 05H 

inicio:  
    ;Nos cambiamos de banco 
	BSF STATUS,RP0 
    BCF STATUS,RP1 
    BSF TXSTA,BRGH ;Bit para validar la tasa de transferencia
    CLRF TRISB ;Limpia TRISB
    MOVLW D'32' ;El 32 indicar� la Baud rate
    MOVWF SPBRG ;se asignan el valor 32 al bit SPBRG
    BCF TXSTA,SYNC ;Configura una comunicaci�n as�crono
    BSF TXSTA,TXEN ;Habilitamos la transmision de datos
    BCF STATUS,RP0 ;Nos cambiamos de banco
    BSF RCSTA,SPEN ;Habilitamos el puerto serie
    BSF RCSTA,CREN ;Habilitamos el receptor

RECIBE: 
  ;Se verifica que llega un dato
  BTFSS PIR1,RCIF 
  GOTO RECIBE
  ;Se guarda el dato en el registro w 
  MOVF RCREG,W 
  ;El valor de Y se pasa a W
  MOVWF Y 

LOOP: 
    CLRF PORTB ;Limpiamos el puerto B
    BCF STATUS,Z ;limpiamos la bandera Z
    MOVF Y,0 ;se lee a Y
    xorlW A'0' ;
    BTFSC STATUS,Z ;si no hay nada, nos vamos a NINGUNO
    GOTO NINGUNO 
    MOVF Y,0 ;Se vuelve a leer Y
    SUBLW A'1' ;Si es un 1
    BTFSC STATUS,Z ;si es verdadero, nos vamos a TODOS
    GOTO TODOS 
    MOVF Y,0 ;Leemos a Y
    SUBLW A'2' ;Si es 2
    BTFSC STATUS,Z ;Verificamos, para ver si nos vamos a Z
    GOTO DERECHA ;
    MOVF Y,0 ;Leemos a Y
    SUBLW A'3' ;comparamos si es un 3 
    BTFSC STATUS,Z ;vemos si la condicion fue cierta para ir a IZQUIERDA
    GOTO IZQUIERDA 
    MOVF Y,0 ;Leemos a Y
    SUBLW A'4' ;comparamos si es un 4
    BTFSC STATUS,Z ;si es cierta la condicion vamos a RL
    GOTO RL 
    MOVF Y,0 ;leemos a Y
    SUBLW A'5' ;Comparamos si es un 5
    BTFSC STATUS,Z ;si hay un 5 en Y, vamos a la funcion ONOFF
    GOTO ONOFF 

NINGUNO:  
    CLRF PORTB ;Limpia PORTB
    MOVLW H'00' ; W = 0
    MOVWF PORTB ;Colocamos w a PORTB  = apagar los bits
    GOTO RECIBE2 

TODOS: 
   CLRF PORTB ;Limpia PORTB
   MOVLW H'FF' ;W = FF
   MOVWF PORTB ; Colocamos w a PORTB = prender todos los bits
   GOTO  RECIBE2 

DERECHA 
   MOVLW H'80' ; w=80
   MOVWF G ;Corrimiento del bit hacia la derecha
   MOVWF PORTB ; PORTB = W
   BCF   STATUS,C 

DERECHA2: 
   RRF   G,0 ;Segundo corrimiento hacia la derecha
   MOVWF G ;w = g
   MOVF  G,0 ;Leemos lo que tiene G
   MOVWF PORTB ; PORTB = W
   CALL  RETARDO ;Llamamos un retardo 
   MOVF  G,0 ;Lectura de g
   SUBLW H'01' ;�g=1?
   BTFSS STATUS,Z 
   GOTO  DERECHA2 ; Si g=1 corrimiento a la derecha
   GOTO  RECIBE2 

IZQUIERDA 
   MOVLW H'01' ;w=1
   MOVWF G ;g=w
   MOVWF PORTB ; PORTB = W
   CALL  RETARDO ;Hacemos un retardo
   BCF   STATUS,C ;Limpiamos C
 
IZQUIERDA2: 
   RLF   G,0 ;Segundo corrimiento a la izquierda
   MOVWF G ;w=g
   MOVF  G,0 ;Se lee g
   MOVWF PORTB ;Limpiamos PORTB
   CALL  RETARDO ;Llamamos un retardo
   MOVF  G,0 ;Leemos lo que tiene G
   BCF   STATUS,C 
   ADDLW H'80' ;+80 para hacer otro corrimiento
   BTFSS STATUS,Z 
   GOTO  IZQUIERDA2 ;Corrimiento a la izquiera otra vez
   GOTO  RECIBE2  

RL: 
   MOVLW  H'80' ;w=80
   MOVWF  G ;G=w
   BCF   STATUS,C ;Limpiamos el registro C 
   MOVWF PORTB ; W=80

DER: 
   RRF  G,0 ;Corrimiento a la derecha
   MOVWF G ;w=g
   MOVF G,0 ;se lee lo que tiene G 
   MOVWF PORTB ;portb=w
   CALL RETARDO ;Hacemos un retardo 
   MOVF G,0 ;Validamos lo que tiene G
   SUBLW H'01' ;Si aun no hay corrimiento
   BTFSS STATUS,Z 
   GOTO  DER ;un corrimiento m�s
   GOTO  IZQUIERDA ;Corrimiento a la izquierda

ONOFF: 
   MOVLW  H'FF' ;w=ff = todos los bits prendidos 
   MOVWF  PORTB ; portb=w
   CALL   RETARDO ;Hacemos un retardo
   MOVLW  H'00' ;w=00=todos los bits apagados
   MOVWF  PORTB  ;portb=w
   CALL   RETARDO ;Hacemos un retardo
   GOTO   RECIBE2 

RECIBE2: 
  MOVF RCREG, W ;Se guarda el dato recibido en w
  MOVWF TXREG ; Carga W al registro de transmision  
  BSF STATUS,RP0;Cambio de banco para verificar transmision  
 
TRANSMITE: 
  BTFSS TXSTA, TRMT;Verificamos si ya se transmitio un dato   
  ;trmt=0 ->Falta transmitir  
  ;trmt=1 ->completo la transmision  
  GOTO TRANSMITE  ;Falta transmitir
  BCF STATUS,RP0;Prepara la recepcion  
  GOTO RECIBE ;Transmisi�n completa

RETARDO: 
   ;El valor de la cte1 se pasa al registro w
   MOVLW  cte1 
   ;valor1 toma el valor de cte1 
   MOVWF  VALOR1 

TRES: 
   ;El valor de la cte2 se pasa al registro w 
   MOVLW  cte2 
   ;valor2 toma el valor de cte2 con ayuda de w
   MOVWF  VALOR2 

DOS:  
   ;El valor de la ct3 se pasa al registro w
   MOVLW  cte3 
   ;valor3 toma el valor de ct3 con ayuda de w
   MOVWF  VALOR3 

UNO: 
   ;Realiza el decremento del valor3
   DECFSZ VALOR3 
   ;Llamada a la rutina uno
   GOTO   UNO 
   ;Realiza el decremento del valor2
   DECFSZ VALOR2 
   ;Llamada a la rutina dos
   GOTO   DOS
   ;Realiza un decremento del valor1 
   DECFSZ VALOR1
   ;Llamada a la rutina tres 
   GOTO   TRES 
   RETURN 
FIN: 
   END
