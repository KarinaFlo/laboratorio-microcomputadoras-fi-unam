processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC

;Carga vector de reset
org 0
  goto inicio
org 6
inicio ;Declaracion de la funci�n inicio 
  ;Limpia el puerto A, listo para usarse
  CLRF PORTA 
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca un 0 al bit RP1
  BCF STATUS, RP1
  ;Se coloca 011x en W para indicar que los puertos son digitales
  MOVLW 06H 
  ;Configura los puertos A y B como digitales
  MOVF ADCON1
  ;Se establece el puerto A como entrada
  MOVLW 3FH
  MOVWF TRISA
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  BCF STATUS,RP0

puertoA: 
    movf PORTA,0 ;Agregamos un cero al puerto B
	xorlw h'00' ;con la funcion XOR comparamos 00
	btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
	call paso1 
	movf PORTA,0
	xorlw h'02' ;con la funcion XOR comparamos 02
	btfsc STATUS,Z  ;Si z es igual a cero, hacemos un salto
	call paso2
	movf PORTA,0 ;Agregamos un cero al puerto B
	xorlw h'04' ;con la funcion XOR comparamos 02
    btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
    call paso3
    movf PORTA,0 ;Agregamos un cero al puerto B
    xorlw h'08' ;con la funcion XOR comparamos 08
    btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
    call paso4 
    movf PORTA,0 ;Agregamos un cero al puerto B
    xorlw h'10' ;con la funcion XOR comparamos 10
    btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
    call paso5
    goto inicio
    
;Los motores estan detenidos    
paso1:
  ;coloca un cero en PORTB
  movlw H'00' ;coloca un 00 al puertoB
  movwf PORTB
  RETURN

;Solo el motor 2 gira en sentido horario
paso2:
  movlw h'03' ;coloca un 03 al puertoB
  movwf PORTB
  RETURN
;Solo el motor 2 gira en sentido antihorario  
paso3:
  movlw h'02' ;coloca un 02 al puertoB
  movwf PORTB
  RETURN
;Solo el motor 1 gira en sentido horario 
paso4:
  movlw h'0C' ;coloca un 0C al puertoB
  movwf PORTB
  RETURN
;Solo el motor 1 gira en sentido antihorario
paso5:
  movlw b'00001000' ;coloca b'00001000' a la salida
  movwf PORTB
  RETURN 

end ;fin del programa
