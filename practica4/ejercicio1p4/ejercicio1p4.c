processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC

;Carga vector de reset
org 0
  ;Llamada a la rutina de inicio
  goto inicio
;Direcci�n de inicio del programa
org 5
inicio:
  CLRF PORTA
  BSF STATUS RP0
  BCF STATUS RP1
  MOVLW 06H
  MOVWF ADCON1
  ;Puerto a es una entrada 
  MOVLW 3FH
  MOVWF TRISA
  MOVLW 00H
  MOVWF TRISB
  BCF STATUS, RP0

puertoA: movwf PORTA, 0
	xorlw h'00'
	btfsc STATUS, Z
	call paso1
	movf PORTA,0
	xorlw h'02'
	btfsc STATUS, Z
	call paso2
	movf PORTA, 0
	xorlw h'04'
    btfsc STATUS,Z
    call paso3
    movf PORTA,0
    xorlw h'08'
    btfsc STATUS,Z
    call paso4
    movf PORTA,0
    xorlw h'10'
    btfsc STATUS,Z
    call paso5
    goto inicio
    
    
paso1:
  ;coloca un cero en PORTB
  movlw H'00' 
  movwf PORTB
  RETURN
	
paso2:
  movlw h'03'
  movwf PORTB
  RETURN
  
paso3:
  movlw h'02'
  movwf PORTB
  RETURN
  
paso4:
  movlw h'0C'
  movwf PORTB
  RETURN

paso5:
  movlw b'00001000' ;coloca b'00001000' a la salida
  movwf PORTB
  RETURN 

end ;fin del programa
