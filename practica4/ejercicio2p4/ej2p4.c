include <p16f877.inc>
	org 00H
	goto inicio 
	org 05H



inicio CLRF PORTA
	BSF STATUS, RP0
	BCF STATUS, RP1
	MOVLW 06H
	MOVWF ADCON1
	MOVLW 3FH
	MOVWF TRISA
	MOVLW 00H
	MOVWF TRISB
	BCF STATUS, RP0

puertoA: movf PORTA, 0
	worlw h'00'
	btfsc STATUS, Z
	call paso1

	movf PORTA, 0
	worlw h'01'
	btfsc STATUS, Z
	call paso2
	
	movf PORTA, 0
	xorlw h'03'
	btfsc STATUS, Z
	call paso 3
	
	movf PORTA, 0	
	xorlw h'03'
	btfsc STATUS, Z
	call paso 4

	movf PORTA, 0 ;realiza operacion con 04h
	xorlw h'04'
	btfsc STATUS, Z
	call paso5
	goto inicio

paso1: movlw h'00'
	movwf PORTB
	RETURN 

paso2: movlw h'0F'
	MOVWF PORTB
	RETURN

paso3: movlw h'0A'
	movwf PORTB
	RETURN

paso4: movlw h'0E'
	movwf PORTB
	RETURN
 
paso5: movlw h'0B'
	movlw PORTB
	RETURN

end
