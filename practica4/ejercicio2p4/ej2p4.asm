;Laboratorio de Microcomputadoras
;Pr�ctica 4 ejercicio 2
;Se carga la biblioteca para el PIC
include <p16f877.inc>
;Para resetear el microprocesador
org 0 
;Carga al vector de RESET la direcci�n de inicio
goto inicio 
;Llama a la funcion inicio
org 6
;Direcci�n de inicio del programa

inicio ;Declaracion de la funci�n inicio 
  ;Limpia el puerto A, listo para usarse
  CLRF PORTA 
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca un 0 al bit RP1
  BCF STATUS, RP1
  ;Se coloca 011x en W para indicar que los puertos son digitales
  MOVLW 06H 
  ;Configura los puertos A y B como digitales
  MOVF ADCON1
  ;Se establece el puerto A como entrada
  MOVLW 3FH
  MOVWF TRISA
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  BCF STATUS,RP0

puertoA: ;Declaracion de la funcion puertoA 
  movf PORTA,0
  xorlw h'00' ;con la funcion XOR comparamos 00
  btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
  ;Manda a llamar a paso1
  call paso1
  movf PORTA,0
  xorlw h'01' ;con la funcion XOR comparamos 01
  btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
  ;Manda a llamar a paso2
  call paso2
  movf PORTA, 0
  xorlw h'02' ;con la funcion XOR comparamos 02
  btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
  ;Manda a llamar a paso3
  call paso3
  movf PORTA,0	
  xorlw h'03' ;con la funcion XOR comparamos 03
  btfsc STATUS,Z 
  ;Manda a llamar a paso4
  call paso4
  movf PORTA,0 ;realiza operacion con 04h
  xorlw h'04' ;con la funcion XOR comparamos 04
  btfsc STATUS,Z ;Si z es igual a cero, hacemos un salto
  ;Manda a llamar a paso5
  call paso5
  goto inicio

paso1:
  ;Coloca un 00 a la salida dle motor
  ;Ambos motores est�n detenidos
  movlw h'00'
  movwf PORTB
  RETURN 

paso2: 
  ;Coloca un 0F a la salida del motor
  ;Ambos motores giran a la derecha
  movlw h'0F'
  MOVWF PORTB
  RETURN

paso3: 
  ;Coloca un 0A en la salida del motor
  ;Ambos motores giran a  la izquierda
  movlw h'0A'
  movwf PORTB
  RETURN

paso4:
  ;Coloca un 0B en la salida del motor
  ;Un motor gira a la derecha y uno a la izquierda
  movlw h'0E'
  movwf PORTB
  RETURN
 
paso5: 
  ;Coloca un 0B en la salida del motor
  ;Un motor gira a la izquierda y uno a la derecha
  movlw h'0B'
  movlw PORTB
  RETURN

end
