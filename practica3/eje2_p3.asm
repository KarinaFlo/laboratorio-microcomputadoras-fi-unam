;Laboratorio de Microcomputadoras
;Pr�ctica 3 ejercicio 2
include <p16f877.inc> ;Se carga la biblioteca para el PIC
;Se reserva espacio en para contador en la direcci�n de memoria 20
contador equ h'20'
;Se reserva espacio para valor1 en la direcci�n 21
valor1 equ h'21'
;Se reserva espacio para valor2 en la direcci�n 22
valor2 equ h'22'
;Se reserva espacio para valor3 en la direcci�n 23
valor3 equ h'23'
;Las constantes ayudaran al retardo de medio segundo
;cte1 toma el valor de 20h
cte1 equ 20h
;cte2 toma el valor de 50h
cte2 equ 50h
;ct3 toma el valor de 60h 
cte3 equ 60h
;Para resetear el microprocesador
org 0H ;Carga al vector de RESET la direcci�n de inicio
goto inicio ;Llama a la funcion inicio
org 05H ;Direcci�n de inicio del programa

inicio:  ;Declaracion de la funci�n inicio 
	;Limpia el puerto A, listo para usarse
    CLRF PORTA 
    ;Nos movemos al banco1 para usar ADCON1
	;El cambio de banco se hace con RP0 Y RP1 de STATUS
	;(RP1,RP0) = (0,1) = Banco1
	;Se coloca un 1 al bit RP0
	BSF STATUS, RP0
	;Se coloca un 0 al bit RP1
	BCF STATUS, RP1
	;Se coloca 011x en W para indicar que los puertos son digitales
	MOVLW 06H 
	;Configura los puertos A y B como digitales
	MOVF ADCON1
	;Se establece el puerto A como entrada
	MOVLW 3FH
	MOVWF TRISA
	;Se establece el puerto B como salida
	MOVLW 00H
	MOVWF TRISB

	;Casos de recorridos que se pueden realizar seg�n la entrada
	ini: ;Declaraci�n de la funci�n ini
	;Evalua lo que hay en b2 del puerto A
	btfss PORTA, 2
	;Si hay un 0 brinca a 0xx
	goto a0xx
	;Si hay un 1 entonces brinca a 1xx
	goto a1xx
	;Rutina para a0xx
	
	;Corresponde a los primeros 4 casos de la tabla en binario
	a0xx:
	;Evalua lo que hay en b1 del puerto A
	btfss PORTA, 1
	;Si hay un 0 brinca a 00x
	goto a00x
	;Si hay un 1 brinca a 01x
	goto a01x

	;Corresponde a los primeros dos casos de la tabla en binario
	a00x:
	;Evalua lo que hay en b0 del puerto A
	btfss PORTA, 0
	;Si hay un 0 brinca a Opcion1
	goto Opcion1
	;Si hay un 1 brinca a Opcion2
	goto Opcion2

	;Corresponde al caso 3 y 4 de la tabla en binario 
	a01x:
	;Evalua lo que hay en b1 del puerto A
	btfss PORTA, 1
	;Si hay 0 brinca a Opcion3
	goto Opcion3
	;Si hay 1 brinca a Opcion4
	goto Opcion4

	;Corresponde al caso 5 y 6 de la tabla en binario
	a1xx: 
	;Evalua lo que hay en b0 del puerto A
	btfss PORTA, 0
	;Si hay 0 brinca a Opcion5
	goto Opcion5
	;Si hay un 1 brinca a Opcion6
	goto Opcion6

	;Declaraci�n de la rutina de Opcion1
	;Corresponde al apagado de todos los leds
	Opcion1:
	;Limpia todo portb para apagar todos los bits
	clrf PORTB
	;Salta a ini
	goto ini

	;Declaraci�n de la rutina de Opcion2
	;Solo un bit est� prendido
	Opcion2:
	;Se coloca un 1 en W
	movlw H'FF'
	;Se prende el segundo bit de portb
	movwf PORTB
	;Salta a ini
	goto ini

	;Declaraci�n de la rutina de Opcion3
	Opcion3:
	;Limpia todo portb para apagarlos leds
	clrf PORTB
	;Coloca un 1 en el b7 de portb
	bsf PORTB, 7
	;Salta a la rutina d_der
	goto d_der

	;Declaraci�n de la rutina d_der
	d_der:
	;Hace un desplzamiento hacia la derecha
	rrf PORTB
	;Manda a llamar al retardo para visualizar la salida
	call retardo
	;Verifica que haya terminado el recorrido
	btfss PORTB,0
	;Salta a d_der otra vez
	goto d_der
	;Salta a la rutina de ini
	goto ini
	
	;Declaraci�n de la rutina Opcion4
	Opcion4:
	;Limpia todo el portb
	clrf PORTB
	;Coloca un 1 en el b0 de portb
	BSF PORTB, 0
	;Salta a la rutina i_der
	goto i_der

	;Declaraci�n de la rutina i_der
	i_der:
	;Hace un desplazamiento a la izquierda
	rlf PORTB, 1
	;Manda a llamar al retardo para visualizar la salida
	call retardo
	;Evalua lo que hay en b7 del portb 
	btfsc PORTB, 7
	;Salta a la rutina i_der
	goto i_der
	;Salta a la rutina ini
	goto ini

	;Declaraci�n de la rutina Opcion5
	Opcion5:
	;Limpia todo lo que hay en portb
	clrf PORTB
	;Coloca un 1 en b7 de portb
	bsf PORTB, 7
	;Salta a la rutina d_der2
	goto d_der2

	;Declaraci�n de la rutina d_der2
	d_der2:
	;Hace un desplazamiento hacia la derecha de portb
	rrf PORTB, 1
	;Manda a llamar al retardo para visualizar la salida
	call retardo
	;Verifica lo que hay en b0 de portb
	btfss PORTB, 0
	;Brinca a la rutina d_der
	goto d_der
	;Brinca a la rutina i_der
	goto i_der 
	
	;Declaraci�n de la rutina Opcion6
	Opcion6:
	;Limpia todo lo que hay en portb
	clrf PORTB
	;Manda a llamar al retardo para visualizar la salida
	call retardo
	;Coloca un uno en w
	movlw H'FF'
	;Coloca un 1 en Portb
	movwf PORTB
	;Manda a llamar al retardo para visualizar la salida
	call retardo
	;Salta a la rutina de ini
	goto ini

	;Declaraci�n de la rutina retardo
	retardo
		;El valor de la cte1 se pasa al registro w
		MOVLW cte1
 		;El valor de la cte1 se pasa al registro w
 		movlw cte1
 		;valor1 toma el valor de cte1 con ayuda de w
 		movwf valor1
		;Definicion de la rutina tres
		tres:
 			;El valor de la cte2 se pasa al registro w 
 			movlw cte2
 			;valor2 toma el valor de cte2 con ayuda de w
 			movwf valor2
		;Definicion de la rutina dos
		dos
 			;El valor de la ct3 se pasa al registro w
 			movlw cte3
 			;valor3 toma el valor de ct3 con ayuda de w
 			movwf valor3
		;Definicion de la rutina uno
		uno 
 			;Realiza el decremento del valor3
 			decfsz valor3
 			;Llamada a la rutina uno
 			goto uno
			 ;Realiza el decremento del valor2
 			decfsz valor2
 			;Llamada a la rutina dos
 			goto dos
 			;Realiza un decremento del valor1
			decfsz valor1
 			;Llamada a la rutina tres
			 goto tres
	return

end ; fin de programa
