;Laboratorio de microcomputadoras
;Practica 3 ejercicio 1
include <p16f877.inc> ;Biblioteca para el PIC16F877
;Para resetear el microprocesador
org 0H ;Carga al vector de RESET la direcci�n de inicio
goto inicio ;Llama a la funcion inicio
org 05H ;Direcci�n de inicio del programa

inicio:  ;Declaracion de la funci�n inicio 
	;Limpia el puerto A, listo para usarse
    CLRF PORTA 
    ;Nos movemos al banco1 para usar ADCON1
	;El cambio de banco se hace con RP0 Y RP1 de STATUS
	;(RP1,RP0) = (0,1) = Banco1
	;Se coloca un 1 al bit RP0
	BSF STATUS, RP0
	;Se coloca un 0 al bit RP1
	BCF STATUS, RP1
	;Se coloca 011x en W para indicar que los puertos son digitales
	MOVLW 06H 
	;Configura los puertos A y B como digitales
	MOVF ADCON1
	;Se establece el puerto A como entrada
	MOVLW 3FH
	MOVWF TRISA
	;Se establece el puerto B como salida
	MOVLW 00H
	MOVWF TRISB
	;Nos cambiamos al banco 0 para visualizar la salida del PORT
 	;RP0 se pone en 0
	BCF STATUS, RP0

ciclo: ;Declaracion de la funci�n ciclo
;Evalua si hay un 1 en el b0 del puerto A
;Si el dipswitch est� en 0 apago los leds de PORTB
;Si est� en 1 enciende los leds de PORTB
btfsc PORTA, 0
 ;Llamada a la rutina de encendido
 goto encendido
;Limpia el puerto B, listo para usarse
clrf PORTB
;Llamada a la rutina ciclo
goto ciclo

encendido: ;Declaracion de la funci�n encendido
;Pone un 1 en todos los bits del puerto
movlw H'FF'
;El valor de w se pasa a PORTB
movwf PORTB
;Llamada a la rutina ciclo
goto ciclo
end
