processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC
J EQU 0X20 ;localidad para la subrutina de retraso
K EQU 0X21 ;Carga al vector RESET la direccion de inicio

goto inicio
org 05H ;Direccion de inicio del programa del usuario
;Declaraci�n de la funci�n de inicio
inicio:
  ;Limpia el puerto A, listo para usarse 
  CLRF PORTA
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca un 0 al bit RP1
  BCF STATUS, RP1
  ;Configuraci�n de PORTA y PORTB como analogicos
  MOVLW 00H
  MOVWF ADCON1
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  ;Regresa al banco 0
  BCF STATUS, RP0
  ;Establece frecuencia del reloj
  movlw b'11001001'
  movwf ADCON0
  ;Limpia el PORB
  CLRF PORTB

INICIO
  ;Se limpia el carry
  BCF STATUS, C 
  ;Se inicia la conversi�n anaogico-digital
  BSF ADCON0,2
  ;Retardo para que se haga la conversi�n
  CALL retardo
  ;Fin de la conversi�n
  BCF ADCON0,2 
  ;Se lee el valor de la conversi�n
  MOVWF ADRESH 
  ;Se resta el primero tercio
  SUBLW H'55' 
  ;Se limpia C
  BTFSC STATUS, C 
  ;Si es <0 va a op1
  GOTO OP1
  ;Se lee el valor de la conversi�n
  MOVFW ADRESH
  ;Se resta el segundo tercio
  SUBLW H'AC' 
  ;Si es >0 se apaga
  BTFSC STATUS, C
  ;si es positivo salta a op2
  GOTO OP2
  ;Si no es ninguna, entonces est� en el tercer tercio
  MOVLW H'07' 
  ;PORTB=7
  MOVFW PORTB
  GOTO INICIO

OP1 MOVLW H'01' ;CARGA UN 1 EN EL PUERTO B
	MOVWF PORTB 
GOTO INICIO

OP2 MOVLW H'03' ;CARGA UN 3 EN EL PUERTO B
	MOVWF PORTB 
GOTO INICIO


;Rutina de retardo de 20 milisegundos
retardo:
  ;W=25
  MOVLW D'25'
  ;J=W 
  MOVWF J 
;Definici�n de rutina
jloop:
  ;K=W
  MOVWF K
;Definicion de rutina
kloop: 
  ;K Decrece
  ;K=K-1 OMITE SI ES CERO
  DECFSZ K,F 
  ;Llamada a kloop
  GOTO kloop
  ;J Decrece
  ;J = J -1 OMITE SI ES CERO
  DECFSZ J, f 
  ;Llamadda a jloop
  GOTO jloop
  RETURN
END

