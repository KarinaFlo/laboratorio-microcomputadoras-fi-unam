processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC
J EQU 0X20 ;localidad para la subrutina de retraso
K EQU 0X21 ;Carga al vector RESET la direccion de inicio

goto inicio
org 05H ;Direccion de inicio del programa del usuario
;Declaración de la función de inicio
inicio:
  ;Limpia el puerto A, listo para usarse 
  CLRF PORTA
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca un 0 al bit RP1
  BCF STATUS, RP1
  ;Configuración de PORTA y PORTB como analogicos
  MOVLW 00H
  MOVWF ADCON1
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  ;Regresa al banco 0
  BCF STATUS, RP0
  ;Establece frecuencia del reloj
  movlw b'11001001'
  movwf ADCON0
  ;Limpia el PORB
  CLRF PORTB

INICIO
;Se inicia la conversión anaogico-digital
BSF ADCON0,2
;Retardo para que se haga la conversión
CALL retardo
;Fin de la conversión
BCF ADCON0,2 
;Se lee el valor de la conversión
MOVWF ADRESH 
;Carga la conversión en PORTB
MOVWF PORTB 
;Llamada a inicio
GOTO INICIO

;Rutina de retardo de 20 milisegundos
retardo:
  ;W=25
  MOVLW D'25'
  ;J=W 
  MOVWF J 
;Definición de rutina
jloop:
  ;K=W
  MOVWF K
;Definicion de rutina
kloop: 
  ;K Decrece
  ;K=K-1 OMITE SI ES CERO
  DECFSZ K,F 
  ;Llamada a kloop
  GOTO kloop
  ;J Decrece
  ;J = J -1 OMITE SI ES CERO
  DECFSZ J, f 
  ;Llamadda a jloop
  GOTO jloop
  RETURN
END
