processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC
J EQU 0X20 ;localidad para la subrutina de retraso
K EQU 0X21 ;Carga al vector RESET la direccion de inicio
VE1 EQU 0X22 ;localidad para VE1
VE2 EQU 0X23
VE3 EQU 0X24
goto inicio
org 05H ;Direccion de inicio del programa del usuario
;Declaración de la función de inicio
inicio:
  ;Limpia el puerto A, listo para usarse 
  CLRF PORTA
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca un 0 al bit RP1
  BCF STATUS, RP1
  ;Configuración de PORTA y PORTB como analogicos
  MOVLW 00H
  MOVWF ADCON1
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  ;Regresa al banco 0
  BCF STATUS, RP0
  ;Limpia el PORTB
  CLRF PORTB

INICIO
  ;Lectura del canal 0
  MOVLW b'11000001'
  MOVWF ADCON0
  BCF STATUS,C
  ;Se inicia la conversión anaogico-digital
  BSF ADCON0,2
  ;Retardo para que se haga la conversión
  CALL retardo
  ;Fin de la conversión
  BCF ADCON0,2 
  ;Se lee el valor de la conversión
  MOVWF ADRESH 
  ;Carga la conversión en VE1
  MOVWF VE1
  
  ;Lectura del canal 1
  MOVLW b'11001001'
  MOVWF ADCON0  
  ;Se inicia la conversión anaogico-digital
  BSF ADCON0,2
  ;Retardo para que se haga la conversión
  CALL retardo
  ;Fin de la conversión
  BCF ADCON0,2 
  ;Se lee el valor de la conversión
  MOVWF ADRESH 
  ;Carga la conversión en VE1
  MOVWF VE2
  
  ;Lectura del canal 2
  MOVLW b'11010001'
  MOVWF ADCON0  
  ;Se inicia la conversión anaogico-digital
  BSF ADCON0,2
  ;Retardo para que se haga la conversión
  CALL retardo
  ;Fin de la conversión
  BCF ADCON0,2 
  ;Se lee el valor de la conversión
  MOVWF ADRESH 
  ;Carga la conversión en VE1
  MOVWF VE3

COMP1: 
  ;VE1>VE2 VE1>VE3
  ;W=VE2
  MOVF VE2,W
  ;W=VE2-VE1
  SUBWF VE1,W
  ;SI W<O ENTONCES VA A COMP2
  BTFSC STATUS,C
  GOTO COMP2	
  ;W = VE3
  MOVF VE3,W
  ;W=VE3-VE1
  SUBWF VE1,W
  ;SI W=0 Entonces va a COMP3
  BTFSC STATUS,C 
  GOTO COMP3
  ;SI VE1>VE2 Y VE1>VE3 Entonces PORTB tiene un 1	
  MOVLW H'01'
  ;PORTB=001 de la tabla		
  MOVWF PORTB
  GOTO INICIO

COMP2:
  ;VE2 > VE1 VE2> VE3
  ;W =VE1
  MOVF VE1,W
  ;W=VE1-VE2
  SUBWF VE2,W
  ;Si W<0 entonces va a comp1
  BTFSC STATUS,C 
  GOTO COMP1	
  ;W=VE3
  MOVF VE3,W
  ;W=VE3-VE2
  SUBWF VE2,W	
  ;IF W<0 entonces va a comp3
  BTFSC STATUS,C 
  GOTO COMP3
  ;VE2 > VE1  y VE2> VE3	
  MOVLW H'03'
  MOVWF PORTB
  ;PORTB=011 de la tabla		
  GOTO INICIO

COMP3:
  ;VE3 > VE1 Y  VE3>VE2
  ;W=VE1
  MOVF VE1,W
  ;W=VE3-VE1
  SUBWF VE3,W
  ;IF W<0 entonces va a comp1
  BTFSC STATUS,C
  GOTO COMP1	
  ;W=VE2
  MOVF VE2,W	
  ;W=VE3-VE2
  SUBWF VE3,W
  ;SI W<0 entonces va a comp2
  BTFSC STATUS,C 
  GOTO COMP2	
  ;Si VE3 > VE1 Y  VE3>VE2
  MOVLW H'07'
  ;PORTB=111 de la tabla
  MOVWF PORTB
  GOTO INICIO

;Rutina de retardo de 20 milisegundos
retardo:
  ;W=25
  MOVLW D'25'
  ;J=W 
  MOVWF J 
;Definición de rutina
jloop:
  ;K=W
  MOVWF K
;Definicion de rutina
kloop: 
  ;K Decrece
  ;K=K-1 OMITE SI ES CERO
  DECFSZ K,F 
  ;Llamada a kloop
  GOTO kloop
  ;J Decrece
  ;J = J -1 OMITE SI ES CERO
  DECFSZ J, f 
  ;Llamadda a jloop
  GOTO jloop
  RETURN
END
