processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC
;Carga vector de reset
org 0H
A equ H'26'
goto inicio 
org 05H
inicio:;Declaracion de la funci�n inicio 
  ;Limpia el puerto A, listo para usarse
  CLRF PORTA
  CLRF PORTB
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca 011x en W para indicar que los puertos son digitales
  BCF STATUS, RP1
  ;Se coloca 011x en W para indicar que los puertos son digitales
  MOVLW 06H
  ;Se establece el puerto A como entrada
  MOVWF ADCON1
  MOVLW 3FH
  MOVWF TRISA
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  BCF STATUS, RP0
  XOR:
  ;coloca lo que hay en el puerto A en 0
  movf PORTA, 0 
  ; despues se coloca en uan variable extra para efectuar el filtro con la mascara
  movwf A
  ;se define la mascara
  movlw D'15' 
  ;se filtra la informacion para evitar basura
  andwf A,f 
  movfw A
  ;Se hace la operacion xor para comparar un cero
  xorlw b'00000000'
  ;se verifica si el reultado es 0 
  btfsc STATUS, Z 
  ;manda a llamar a la rutina rutina1
  call rutina1 
  movfw A
  ;se hace la operacion xor para comparar un uno
  xorlw b'00000001' 
  ;se verifica si el reultado es 1
  btfsc STATUS, Z
  ;manda a llamar a la rutina rutina2
  call rutina2
  movfw A
  ;se hace la operacion xor para comparar un dos
  xorlw b'00000010' 
  ;se verifica si el reultado es 2
  btfsc STATUS, Z 
  ;manda a llamar a la rutina rutina3
  call rutina3
  movfw A
  ;se hace la operacion xor para comparar un 8
  xorlw b'000000100' 
  ;se verifica si el reultado es 8
  btfsc STATUS, Z
  ;manda a llamar a la rutina rutina4
  call rutina4
  goto XOR
;motores apagados
rutina1: 
  clrf PORTB
  return 
;motor 1 encendido en sentido horario
rutina2:
  movlw H'0E'
  movwf PORTB
  return
;motor 1 y 2 encendidos en sentido horario
rutina3:
  movlw H'0F'
  movwf PORTB
  return
;motor 1 encendido en sentido antihorario
rutina4:
  movlw H'0B'
  movwf PORTB
  return
END
