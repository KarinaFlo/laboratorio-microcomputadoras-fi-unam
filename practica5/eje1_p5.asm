processor 16f877 ;Se define el PIC a trabajar
include<p16f877.inc> ;Se carga la biblioteca para el PIC
;Carga vector de reset
org 0H
goto inicio
org 05H

inicio ;Declaracion de la funci�n inicio 
  ;Limpia el puerto A, listo para usarse
  CLRF PORTA 
  ;Nos movemos al banco1 para usar ADCON1
  ;El cambio de banco se hace con RP0 Y RP1 de STATUS
  ;(RP1,RP0) = (0,1) = Banco1
  ;Se coloca un 1 al bit RP0
  BSF STATUS, RP0
  ;Se coloca un 0 al bit RP1
  BCF STATUS, RP1
  ;Se coloca 011x en W para indicar que los puertos son digitales
  MOVLW 06H 
  ;Configura los puertos A y B como digitales
  MOVF ADCON1
  ;Se establece el puerto A como entrada
  MOVLW 3FH
  MOVWF TRISA
  ;Se establece el puerto B como salida
  MOVLW 00H
  MOVWF TRISB
  BCF STATUS,RP0
  AND
  ;Mascara para discriminar los bits restantes
  MOVLW b'00000111'
  ;Filtra los bits del puerto A haciendo uso de la mascara
  ANDWF PORTA,W
  ;mueve al puerto b
  MOVWF PORTB
  ;regresa al inicio
  GOTO AND
  END
